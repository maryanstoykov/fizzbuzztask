﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzProgram
{
    public static class FizzBuzz
    {
        public static string[] FizzAndBuzz(int number)
        {
            List<string> FizzBuzzList = new List<string>();

            for (int n = 1; n <= number; n++)
            {
                string ret = "";
                if (n % 3 == 0)
                {
                    ret += "fizz";
                }
                if (n % 5 == 0)
                {
                    ret += "buzz";
                }
                 if (ret.Equals("")) ret = n.ToString();

                FizzBuzzList.Add(ret);
            }
            return FizzBuzzList.ToArray();
        }
    }
}
