﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                int n = 0;
              
                    Console.WriteLine("\nEnter integer number (0<n) ");
                    bool ok = Int32.TryParse(Console.ReadLine(),out n);
                            
                string[] FizzBuzzArray;
                FizzBuzzArray = FizzBuzz.FizzAndBuzz(n);
                foreach (string fzbz in FizzBuzzArray)
                {
                    Console.WriteLine(fzbz);
                }
                Console.WriteLine("\nRun again (y/n)? ");
                n = 0;
            } while (Console.ReadKey().Key != ConsoleKey.N);
           
            Console.ReadLine();
        }
    }
}
