The FizzBuzz Task

"Write a program that asks the user the maximum digit to loop until and return an array with the series of digits from 1 to the entered digit, but instead of numbers divisible by 3, use string ‘fizz’, by 5, use ‘buzz’, and by both, use ‘fizzbuzz’. For example, if we enter 15, we should get an array of 1, 2, ‘fizz’, 4, ‘buzz’, ‘fizz’, 7, 8, ‘fizz’, ‘buzz’, 11, ‘fizz’, 13, 14, ‘fizzbuzz’."
